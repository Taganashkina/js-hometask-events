'use strict';

/*
  Вам необходимо добавить обработчики событий в соотвествии с заданием.
  Вам уже даны готовые стили и разметка. Менять их не нужно,
  за исключением навешивания обработчиков и добавления data - аттрибутов
*/

/**
 * Задание 1
 * Необходимо сделать выпадающее меню.
 * Меню должно открываться при клике на кнопку,
 * закрываться при клике на кнопку при клике по пункту меню
*/
  function openMenu() {
    document.getElementById("menu").classList.toggle("menu-opened");
  }
/**
 * Задание 2
 * Необходимо реализовать перемещение блока по клику на поле.
 * Блок должен перемещаться в место клика.
 * При выполнении задания вам может помочь свойство element.getBoundingClientRect()
*/
  field.onclick = function (event) {
    const fieldPosition = this.getBoundingClientRect();
    movedBlock.style.top = event.clientY - fieldPosition.y + 'px';
    movedBlock.style.left = event.clientX - fieldPosition.x + 'px';
  }

/**
 * Задание 3
 * Необходимо реализовать скрытие сообщения при клике на крестик при помощи делегирования
*/
  messager.onclick = function (event) {
   const message = event.target.closest('.message');
   if (event.target.className != 'remove') 
     return;
   message.style.display = 'none';
  }
/**
 * Задание 4
 * Необходимо реализовать вывод сообщения при клике на ссылку.
 * В сообщении спрашивать, что пользователь точно хочет перейти по указанной ссылке.
 * В зависимости от ответа редиректить или не редиректить пользователя
*/
  document.getElementById("links").addEventListener("click", (event) => {
    const link = event.target.getAttribute('href');
    confirm("Подтвердите переход по внешней ссылке") ? window.location.href = link : false;
    event.preventDefault();
  })
/**
 * Задание 5
 * Необходимо сделать так, чтобы значение заголовка изменялось в соответствии с измением
 * значения в input
*/
  fieldHeader.onkeyup = function (event) {
    const header = document.getElementById('taskHeader');
    const field = document.getElementById('fieldHeader');
    header.innerHTML = "Заголовок " + field.value;
  }